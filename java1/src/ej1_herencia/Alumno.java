package ej1_herencia;

public class Alumno extends UsuarioApp {

	public boolean repetidor;

	public Alumno(String nombre, int edad, char sexo, String nacionalidad, boolean repetidor) {
		this.nombre = nombre;
		this.edad = edad;
		this.sexo = sexo;
		this.nacionalidad = nacionalidad;
		this.repetidor = repetidor;
	}

	public boolean isRepetidor() {
		return repetidor;
	}

	public void setRepetidor(boolean repetidor) {
		this.repetidor = repetidor;
	}

	@Override
	public String toString() {
		String strPadre = super.toString();
		return "Alumno " + strPadre.substring(0, strPadre.length()-1) + ", repetidor=" + repetidor + "]";
	}
	
	
}
