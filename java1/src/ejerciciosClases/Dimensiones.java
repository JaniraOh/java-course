package ejerciciosClases;

public class Dimensiones {
	
	private float x; //ancho
	private float y; //alto
	private float z; //fondo
	
	public Dimensiones() {
		this.x = 0;
		this.y = 0;
		this.z = 0;
	}
	public Dimensiones(float x, float y, float z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}
	public float getAncho() {
		System.out.print("Ancho del paquete: ");
		return x;
	}
	public float getAlto() {
		System.out.print("Alto del paquete: ");
		return y;
	}
	public float getFondo() {
		System.out.print("Fondo del paquete: ");
		return z;
	}
	public void setAncho(float x) {
		this.x = x;
	}
	public void setAlto(float y) {
		this.y = y;
	}
	public void setFondo(float z) {
		this.z = z;
	}
	public float alturaMaximaParaApilar(float alturaMaxima) {
		if (this.y > alturaMaxima) {
			System.out.print("Error. Ni siquiera el primero cabe.");
			return 0;
		} else {
			float altura = alturaMaxima - this.y;
			System.out.print("Altura m�xima de otro paquete para poder apilarlo: ");
			return altura;
		}
	}
	public float sumaDimensionesX(Dimensiones dimensiones) {
		float x = dimensiones.x + this.x;
		System.out.print("La suma del ancho de los dos paquetes es: ");
		return x;
	}
	public float sumaDimensionesY(Dimensiones dimensiones) {
		float y = dimensiones.y + this.y;
		System.out.print("La suma de las alturas de los dos paquetes es: ");
		return y;
	}
	public float sumaDimensionesZ(Dimensiones dimensiones) {
		float z = dimensiones.z + this.z;
		System.out.print("La suma de los fondos de los dos paquetes es: ");
		return z;
	}
	public String toString() {
		return "Dimensiones: x = " + this.x + " y = " + this.y + " z = " + this.z + ".";
	}
	public boolean esPosibleApilarPaquetes(Dimensiones
			dimensionesOtroPaquete, float alturaMaximaParaApilar) {
		boolean esPosible = false;
		if (dimensionesOtroPaquete.y + this.y <= alturaMaximaParaApilar) {
			esPosible = true;
		}
		System.out.print("�Es posible apilar los dos paquetes?: ");
		return esPosible;
	}
	public void voltearDimensionX() {
		float aux = y;
		this.y = z;
		this.z = aux;
	}
	public void voltearDimensionY() {
		float aux = x;
		this.x = z;
		this.z = aux;
	}
	public void voltearDimensionZ() {
		float aux = x;
		this.x = y;
		this.y = aux;
	}
	
	public static void main(String[] args) {
		
		Dimensiones paquete1 = new Dimensiones(1,2,3);
		Dimensiones paquete2 = new Dimensiones(4,5,6);
		
		System.out.println(paquete1.getAncho());
		System.out.println(paquete2.getAlto());
		paquete1.setAncho(2);
		paquete1.setFondo(2);
		System.out.println(paquete1.getAncho());
		System.out.println(paquete1.getFondo());
		
		System.out.println(paquete2.alturaMaximaParaApilar(4));
		System.out.println(paquete1.sumaDimensionesX(paquete2));
		System.out.println(paquete1.esPosibleApilarPaquetes(paquete2, 7));
		paquete2.voltearDimensionX();
		System.out.println(paquete2.toString());
		System.out.println(paquete1.toString());
		System.out.println(paquete1.esPosibleApilarPaquetes(paquete2, 7));
	}
}
