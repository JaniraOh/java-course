package ejercicios;

import java.util.Scanner;

public class Ej5 {
	
	public static void main(String[] args) {
		
		Scanner teclado = new Scanner(System.in);
		System.out.println("Introduce las notas: ");
		
		int muyDef = 0, def = 0, insuf = 0, apr = 0, not = 0, sobre = 0;
		int nota;
		
		for (int i=0;i<10;i++) {
			nota = teclado.nextInt();
			switch (nota) {
			case 0:
				muyDef++;
				break;
			case 1:
			case 2:
				def++;
				break;
			case 3:
			case 4:
				insuf++;
				break;
			case 5:
			case 6:
				apr++;
				break;
			case 7:
			case 8:
				not++;
				break;
			case 9:
			case 10:
				sobre++;
				break;
			}
		}
		
		System.out.println("Resultados:");
		System.out.println("Muy deficientes :" + muyDef);
		System.out.println("Deficientes :" + def);
		System.out.println("Insuficientes :" + insuf);
		System.out.println("Aprobados :" + apr);
		System.out.println("Notables :" + not);
		System.out.println("Sobresalientes :" + sobre);
	
		teclado.close();
	}
}
