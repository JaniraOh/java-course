package ejercicios;

import java.util.Scanner;

public class Ej18 {
	
	public static void main(String[] args) {
		
		Scanner teclado = new Scanner(System.in);
		System.out.println("Introduce un n�mero: ");
		
		int num;
		num = teclado.nextInt();
		
		int n1 = 1;
		int n2 = 1;
		int next = 0;
		
		while (next < num ) {
			next = n1 + n2;
			n1 = n2;
			n2 = next;
		}
		
		System.out.println("El primer elemento de la serie de Fibonacci mayor o igual que el introducido es : " + next);
		
		teclado.close();
	}
	
}
