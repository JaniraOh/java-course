package ejercicios;

import java.util.Scanner;

public class Ej3 {
	
	public static void main(String[] args) {
		Scanner teclado = new Scanner(System.in);
			
		System.out.println("Introduce distancia a recorrer en km: ");
		double distancia;
		distancia = teclado.nextDouble();
		
		System.out.println("Introduce n�mero de d�as de estancia: ");
		int dias;
		dias = teclado.nextInt();
		
		double precio;
		precio = 0.35*distancia;
		
		if (distancia > 1000 && dias > 7) {
			precio = Math.round(precio*0.7);
		}
		
		System.out.println("El precio del billete de ida y vuelta es: " + precio + " euros.");
		
		teclado.close();
	}
}



