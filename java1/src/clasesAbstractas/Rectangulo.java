package clasesAbstractas;

public class Rectangulo extends FiguraGeométrica {

	public float base;
	public float altura;
	
	public Rectangulo(float base, float altura) {
		this.base = base;
		this.altura = altura;
	}
	public float getBase() {
		return base;
	}
	public void setBase(float base) {
		this.base = base;
	}
	public float getAltura() {
		return altura;
	}
	public void setAltura(float altura) {
		this.altura = altura;
	}
	@Override
	public float calcular_area() {
		area = base * altura;
		return area;
	}
}
