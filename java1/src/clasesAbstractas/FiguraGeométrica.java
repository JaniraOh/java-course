package clasesAbstractas;

public abstract class FiguraGeométrica {
	
	public int posX;
	public int posY;
	public float area = 0;
	
	abstract public float calcular_area();
}
