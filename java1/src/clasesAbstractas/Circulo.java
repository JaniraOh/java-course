package clasesAbstractas;

public class Circulo extends FiguraGeométrica {
	
	public float radio;
	
	public Circulo(float radio) {
		this.radio = radio;
	}

	public float getRadio() {
		return radio;
	}

	public void setRadio(float radio) {
		this.radio = radio;
	}
	
	@Override
	public float calcular_area() {
		area = (float) (Math.PI * Math.pow(radio, 2));
		return area;
	}
}
